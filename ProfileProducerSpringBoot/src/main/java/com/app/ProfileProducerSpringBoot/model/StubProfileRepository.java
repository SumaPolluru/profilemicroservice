package com.app.ProfileProducerSpringBoot.model;

import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class StubProfileRepository implements ProfileRepository {

	private Map<String, Profile> profileData = new HashMap<String, Profile>();

	public StubProfileRepository() {
		Profile profile = new Profile("1000", "Ajit");
		// if u give user id it will return whole profile of user agains that key
		profileData.put("1000", profile);
		profile = new Profile("2000", "Amay");
		profileData.put("2000", profile);
		profile = new Profile("3000", "Dinesh");
		profileData.put("3000", profile);
	}

	@Override
	public List<Profile> getAllProfiles() {

		return new ArrayList<Profile>(profileData.values());
	}

	@Override
	public Profile getProfile(String userId) {
		return profileData.get(userId);
	}
}
