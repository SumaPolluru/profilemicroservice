package com.app.ProfileProducerSpringBoot.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.ProfileProducerSpringBoot.model.Profile;
import com.app.ProfileProducerSpringBoot.model.StubProfileRepository;

@RestController
public class ProfileController {
	
	@Autowired
	StubProfileRepository profileRepository;
	
	@RequestMapping("/profiles")
	public Profile[] all() {
		System.out.println("Coming in server");
		List<Profile> profiles = profileRepository.getAllProfiles();
		return profiles.toArray(new Profile[profiles.size()]);
	}
	
	@RequestMapping("/profiles/{id}")
	public Profile byId(@PathVariable("id") String userId) {
		Profile profileop = profileRepository.getProfile(userId);
		//Profile profile=profileop.get();
		return profileop;
	}
}
