package com.app.ProfileConsumer.model;
import java.io.Serializable;

//@Entity
//@Table(name="Profiles")
public class Profile  {
	
	//@Id
	private String userId;
	private String name;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "Profile [userId=" + userId + ", name=" + name + "]";
	}
	
	public Profile() {
		// TODO Auto-generated constructor stub
	}
	
	public Profile(String userId, String name) {
		super();
		this.userId = userId;
		this.name = name;
	}
	
}
