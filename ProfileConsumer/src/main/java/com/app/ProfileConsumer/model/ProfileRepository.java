package com.app.ProfileConsumer.model;

	import java.util.*;

	import org.springframework.stereotype.Repository;
	@Repository
	public interface ProfileRepository{
		List<Profile> getAllProfiles();
		Profile getProfile(String userId);
		
	}
	/*
	 * We can retreive values from database or we can have some dummy values in list
	 */
