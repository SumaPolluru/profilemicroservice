package com.app.ProfileConsumer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.ProfileConsumer.model.ProfileRepository;

@Controller
public class ConsumerController {

	@Autowired
	ProfileRepository profileRepository;
	
	@RequestMapping("/")
	public String home(){
		
		System.out.println("coming here ");
		return "index";//it is going to search index.jsp in views folder under webapp
	}
	
	@RequestMapping("/userProfiles")
	public ModelAndView profileList() {
		System.out.println("Welcome to java");
		
		return new ModelAndView("userProfiles",
				"profiles", profileRepository.getAllProfiles());
	}
	
	@RequestMapping("/userDetails")
	public String profileDetails(@RequestParam("id") String userId, Model model) {
		model.addAttribute("profile", profileRepository.getProfile(userId));
		return "userDetails";
	}
}
